import Koa from 'koa';
import koaBodyMWFactory from 'koa-body';
import koaLoggerMWFactory from 'koa-logger';
import { jwtMWFactory } from './jwt-middleware';
import { notFoundMW } from './not-found-middleware';

export const getKoaApp = (tokenSecret: string) => {
  const app = new Koa();

  app.use(koaLoggerMWFactory());
  app.use(koaBodyMWFactory());
  app.use(jwtMWFactory(tokenSecret));
  app.use(notFoundMW);

  return app;
};
