import { agent } from 'supertest';
import jwt from 'jsonwebtoken';
import { getKoaApp } from './app';
import { getConfig } from './config';
import { Server } from 'net';

interface TestCase {
  name: string;
  route: string;
  expectedStatusCode: number;
  expectedBody?: object;
  isTokenExpected: boolean;
}

const testCases: TestCase[] = [
  {
    name: 'should return 200 and token in body if POST /jwt is called with a user in the request body',
    expectedStatusCode: 200,
    route: '/jwt',
    isTokenExpected: true,
  },
  {
    name: 'should return 404 and Not Found if any other route is called',
    expectedStatusCode: 404,
    route: '/someroute',
    isTokenExpected: false,
    expectedBody: {
      error: 'Not Found',
    },
  },
];

describe('routes', () => {
  let server: Server;
  const config = getConfig();

  beforeAll(() => {
    server = getKoaApp(config.tokenSecret).listen();
  });

  afterAll(() => {
    server.close();
  });

  testCases.forEach(testCase =>
    test(testCase.name, async () => {
      const app = agent(server);

      const payload = {
        id: 'some-id',
        username: 'jozsF',
      };

      const response = await app
        .post(testCase.route)
        .send(payload)
        .expect(testCase.expectedStatusCode);

      if (testCase.isTokenExpected) {
        expect(response.body).toHaveProperty('token', expect.any(String));
        const token = response.body.token;

        const decoded = jwt.verify(token, config.tokenSecret);
        expect(decoded).toEqual(expect.objectContaining(payload));
      } else {
        expect(response.body).toEqual(testCase.expectedBody);
      }
    }),
  );
});
