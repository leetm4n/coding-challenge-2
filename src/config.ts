import convict from 'convict';

const convictConfig = convict({
  port: {
    env: 'PORT',
    default: 2000,
    format: Number,
  },
  tokenSecret: {
    env: 'TOKEN_SECRET',
    default: 'tokenSecret',
    format: String,
  },
});

convictConfig.validate({ allowed: 'strict' });

export const getConfig = () => convictConfig.getProperties();
