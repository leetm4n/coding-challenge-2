import { getKoaApp } from './app';
import { getConfig } from './config';

async function main() {
  try {
    const config = getConfig();
    const app = getKoaApp(config.tokenSecret);

    app.listen(config.port, () => {
      console.log('server started');
    });
  } catch (err) {
    console.error(err);
  }
}

main();
