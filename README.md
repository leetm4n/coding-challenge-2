# Coding Challenge 2

## The task

- Fork the repository
- Solve the problem explained below
- Create a new merge request to this repository with the solution
- The tests have to pass (all checks green)

## The problem

The skeleton within this repository has two files `jwt-middleware.ts` and `not-found-middleware.ts`, extend their code in the following way. The webserver has to return a generated token only on POST /jwt calls, the body shall be provided in the following way:
```json
{
  "id": "user-id",
  "username": "username"
}
```

The response should be a 200 and a body with the same JSON structure:
```json
{
  "token": "the generated token"
}
```

Use `jsonwebtoken` npm package to create the token.

Every other call made to the webserver shall return 404 and the following body:
```json
{
  "error": "Not Found"
}
```
